# https://docs.sciml.ai/DiffEqGPU/stable/manual/ensemblegpuarray/

using DiffEqGPU, OrdinaryDiffEq, StaticArrays

function lorenz(u, p, t)
    σ = p[1]
    ρ = p[2]
    β = p[3]
    du1 = σ * (u[2] - u[1])
    du2 = u[1] * (ρ - u[3]) - u[2]
    du3 = u[1] * u[2] - β * u[3]
    return SVector{3}(du1, du2, du3)
end

u0 = @SVector [1.0f0; 0.0f0; 0.0f0]
tspan = (0.0f0, 10.0f0)
p = @SVector [10.0f0, 28.0f0, 8 / 3.0f0]
prob = ODEProblem{false}(lorenz, u0, tspan, p)
prob_func = (prob, i, repeat) -> remake(prob, p = (@SVector rand(Float32, 3)).*p)
monteprob = EnsembleProblem(prob, prob_func = prob_func, safetycopy = false)

# reccomended default is 10_000, but experimentation seems to be encouraged.
# https://docs.sciml.ai/DiffEqGPU/stable/manual/optimal_trajectories/
traj_count = 100
# GPUTsit5 fails on EnsembleCPUArray.
println("\nGPUTsit5 on EnsembleGPUKernel")
@time sol = solve(monteprob, GPUTsit5(), EnsembleGPUKernel(),trajectories = traj_count)

algorithms = [Tsit5, Heun, Midpoint]
hardware = [EnsembleCPUArray, EnsembleGPUArray]
euler_timestep = 10^-15
for alg in algorithms
    for hw in hardware
    	println('\n', alg, " on ", hw)
    	@time sol = solve(monteprob,alg(), hw(),trajectories=traj_count,saveat=1.0f0)
    end
end
println("\nEuler on EnsembleCPUArray")
@time sol = solve(monteprob,Euler(), EnsembleCPUArray(),trajectories=traj_count,saveat=1.0f0, dt=euler_timestep)
println("\nEuler on EnsembleGPUArray")
@time sol = solve(monteprob,Euler(), EnsembleGPUArray(),trajectories=traj_count,saveat=1.0f0, dt=euler_timestep)
