using OrdinaryDiffEq, LinearAlgebra
using CUDA
using DiffEqGPU

# Per problem.
# Run each problem under GPU and CPU, and then under Tsit5, Heun, and Midpoint for both architectures. 
hardware = [EnsembleCPUArray, EnsembleGPUArray]
algorithms = [Tsit5, Heun, Midpoint]

# https://docs.sciml.ai/DiffEqGPU/stable/getting_started/#Simple-Example-of-Within-Method-GPU-Parallelism
# Simple Example of Within-Method GPU Parallelism
for alg in algorithms
    u0 = rand(1000)
    A  = randn(1000,1000)
    f(du,u,p,t)  = mul!(du,A,u)
    prob = ODEProblem(f,u0,(0.0,1.0))
    sol = solve(prob,alg())

    # for GPU
    u0 = cu(rand(1000))
    A  = cu(randn(1000,1000))
    f(du,u,p,t)  = mul!(du,A,u)
    prob = ODEProblem(f,u0,(0.0f0,1.0f0)) # Float32 is better on GPUs!
    sol = solve(prob,alg())
end

# https://docs.sciml.ai/DiffEqGPU/stable/manual/ensemblegpuarray/
# EnsembleGPUArray
for hw in hardware
    for alg in algorithms
        function lorenz(du,u,p,t)
            du[1] = p[1]*(u[2]-u[1])
            du[2] = u[1]*(p[2]-u[3]) - u[2]
            du[3] = u[1]*u[2] - p[3]*u[3]
        end

        u0 = Float32[1.0;0.0;0.0]
        tspan = (0.0f0,100.0f0)
        p = [10.0f0,28.0f0,8/3f0]
        prob = ODEProblem(lorenz,u0,tspan,p)
        prob_func = (prob,i,repeat) -> remake(prob,p=rand(Float32,3).*p)
        monteprob = EnsembleProblem(prob, prob_func = prob_func, safetycopy=false)
        @time sol = solve(monteprob,alg(),hw(),trajectories=10_000,saveat=1.0f0)
    end
end
