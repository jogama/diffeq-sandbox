# https://docs.sciml.ai/DiffEqGPU/stable/getting_started/

using OrdinaryDiffEq, LinearAlgebra
using CUDA
using Profile, ProfileSVG, Dates

problem_size = 1000
test_time = string(
    Dates.format(
        Dates.unix2datetime(time()), "yyyy-mm-dd-HH:MM"
    )
)
svg_name(size, hardware) = test_time * "-n" * string(size) * '-' * hardware * ".svg"

function run_examples(n, hw)

    println("Running examples with square arrays of size ", n)

    if hw == "cpu"
        println("on CPU")
        u0 = rand(n)
        A  = randn(n,n)
        f(du,u,p,t)  = mul!(du,A,u)
        prob = ODEProblem(f,u0,(0.0,1.0))
        if n > 1
            @profview sol = solve(prob,Tsit5())
            ProfileSVG.save(svg_name(n, hw))
        else
            println("warming up...")
            sol = solve(prob,Tsit5())
        end

    # Translating this to a GPU-based solve of the ODE simply requires moving the arrays for the initial condition, parameters, and caches to the GPU. This looks like:

    elseif hw == "cuda"
        println("on CUDA")
        u0 = cu(rand(n))
        A  = cu(randn(n,n))
        g(du,u,p,t) = mul!(du,A,u)
        prob = ODEProblem(g,u0,(0.0f0,1.0f0)) # Float32 is better on GPUs!

        Profile.clear();
        # defaults are n=10^6 instr ptrs, delay=0.001 seconds. 
        Profile.init(n=10^7, delay=0.01) 
        @profile sol = solve(prob,Tsit5())
        ProfileSVG.save(svg_name(n, hw))
    end
end

run_examples(1, "cpu") # run once to trigger compilation
run_examples(problem_size, "cpu")
run_examples(problem_size, "cuda")
